package aws_ecs_utils

import "time"

type ECSTaskStateChangeDetailType struct {
	ClusterArn           string `json:"clusterArn"`
	ContainerInstanceArn string `json:"containerInstanceArn"`
	Containers           []struct {
		ContainerArn    string `json:"containerArn"`
		LastStatus      string `json:"lastStatus"`
		Name            string `json:"name"`
		RuntimeID       string `json:"runtimeId"`
		NetworkBindings []struct {
			BindIP        string `json:"bindIP"`
			ContainerPort int    `json:"containerPort"`
			HostPort      int    `json:"hostPort"`
			Protocol      string `json:"protocol"`
		} `json:"networkBindings"`
		TaskArn           string        `json:"taskArn"`
		NetworkInterfaces []interface{} `json:"networkInterfaces"`
		CPU               string        `json:"cpu"`
		Memory            string        `json:"memory"`
	} `json:"containers"`
	CreatedAt     time.Time `json:"createdAt"`
	LaunchType    string    `json:"launchType"`
	CPU           string    `json:"cpu"`
	Memory        string    `json:"memory"`
	DesiredStatus string    `json:"desiredStatus"`
	Group         string    `json:"group"`
	LastStatus    string    `json:"lastStatus"`
	Overrides     struct {
		ContainerOverrides []struct {
			Name string `json:"name"`
		} `json:"containerOverrides"`
	} `json:"overrides"`
	Attachments       []interface{} `json:"attachments"`
	Connectivity      string        `json:"connectivity"`
	ConnectivityAt    time.Time     `json:"connectivityAt"`
	PullStartedAt     time.Time     `json:"pullStartedAt"`
	StartedAt         time.Time     `json:"startedAt"`
	StartedBy         string        `json:"startedBy"`
	PullStoppedAt     time.Time     `json:"pullStoppedAt"`
	UpdatedAt         time.Time     `json:"updatedAt"`
	TaskArn           string        `json:"taskArn"`
	TaskDefinitionArn string        `json:"taskDefinitionArn"`
	Version           int           `json:"version"`
}
